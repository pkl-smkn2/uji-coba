<?php

# routing untuk homepage

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
  return view('home');
});

Route::get('/user', 'UserController@index');

Route::group(['miiddleware'=> 'auth'], function()
{
  Route::get('profile', 'ProfileController@edit');
});

Route::post('/upload', 'UserController@uploadAvatar');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');